#pragma once

#include <chrono>

typedef std::chrono::high_resolution_clock hr_clock;
typedef std::chrono::nanoseconds clock_freq;

struct Timer
{
	inline void reset() { start = hr_clock::now(); }

	[[nodiscard]] inline double elapsed() const
	{
		return std::chrono::duration_cast<std::chrono::duration<double>>(hr_clock::now() - start).count();
	}

	[[nodiscard]] inline double elapsed_millis() const { return elapsed() * 1000.0f; }

private:
	hr_clock::time_point start = hr_clock::now();
};
