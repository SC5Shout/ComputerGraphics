#pragma once

#include <iostream>
#include <vector>
#include <SDL_opengl.h>
#include <cassert>
#include <memory>

typedef void* ImTextureID;

enum struct Filtering
{
	Nearest,
	Linear
};

template<typename T>
concept pixel_type = std::is_same_v<T, uint8_t> || std::is_same_v<T, uint32_t>;

struct Texture
{
	[[nodiscard]] static inline std::shared_ptr<Texture> create()
	{
		return std::make_shared<Texture>();
	}

	[[nodiscard]] static inline std::shared_ptr<Texture> create(const uint32_t w, const uint32_t h)
	{
		return std::make_shared<Texture>(w, h);
	}

	template<typename PixelT>
	[[nodiscard]] static inline std::shared_ptr<Texture> create(std::vector<PixelT>&& pixels, const uint32_t w, const uint32_t h)
	{
		return std::make_shared<Texture>(std::move(pixels), w, h);
	}

	template<typename PixelT>
	[[nodiscard]] static inline std::shared_ptr<Texture> create(const std::vector<PixelT>& pixels, const uint32_t w, const uint32_t h)
	{
		return std::make_shared<Texture>(pixels, w, h);
	}

	Texture()
	{
		create_internal();
	}

	Texture(const uint32_t w, const uint32_t h)
		: w(w), h(h)
	{
		//assert(w > 1, "Width has to be greater than 0");
		//assert(h > 1, "Height has to be greater than 0");

		create_internal();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	}

	template<pixel_type PixelT>
	Texture(std::vector<PixelT>&& pixels, const uint32_t w, const uint32_t h)
		: w(w), h(h)
	{
		//assert(w > 1, "Width has to be greater than 0");
		//assert(h > 1, "Height has to be greater than 0");

		create_internal();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	}

	template<pixel_type PixelT>
	Texture(const std::vector<PixelT>& pixels, const uint32_t w, const uint32_t h)
		: w(w), h(h)
	{
		//assert(w > 1, "Width has to be greater than 0");
		//assert(h > 1, "Height has to be greater than 0");

		create_internal();
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	}

	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;

	Texture(Texture&&) = delete;
	Texture& operator=(Texture&&) = delete;

	~Texture()
	{
		glDeleteTextures(1, &texture);
	}

	inline void resize(const uint32_t w, const uint32_t h)
	{
		//assert(w > 1, "Width has to be greater than 0");
		//assert(h > 1, "Height has to be greater than 0");

		if (this->w == w && this->h == h) {
			return;
		}

		this->w = w;
		this->h = h;
		glDeleteTextures(1, &texture);

		create_internal();

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	}

	template<pixel_type PixelT>
	void add_data(std::vector<PixelT>&& pixels, const uint32_t w, const uint32_t h, int x = 0, int y = 0) 
	{
		//assert(w > 1, "Width has to be greater than 0");
		//assert(h > 1, "Height has to be greater than 0");

		glBindTexture(GL_TEXTURE_2D, texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	}

	template<pixel_type PixelT>
	void add_data(const std::vector<PixelT>& pixels, const uint32_t w, const uint32_t h, int x = 0, int y = 0)
	{
		//assert(w > 1, "Width has to be greater than 0");
		//assert(h > 1, "Height has to be greater than 0");

		glBindTexture(GL_TEXTURE_2D, texture);
		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	}

	[[nodiscard]] inline uint32_t get_w() const { return w; }
	[[nodiscard]] inline uint32_t get_h() const { return h; }

	operator ImTextureID() const { return (void*)texture; }

private:
	void create_internal()
	{
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering == Filtering::Nearest ? GL_NEAREST : GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering == Filtering::Nearest ? GL_NEAREST : GL_LINEAR);
	}

	uint32_t w = 1;
	uint32_t h = 1;
	Filtering filtering = Filtering::Nearest;

	GLuint texture = 0;
};