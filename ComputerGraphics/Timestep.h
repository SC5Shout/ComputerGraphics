#pragma once

#include <compare>

struct Timestep
{
	Timestep() = default;
	Timestep(double time) : time(time) {}

	operator double() const { return time; }

	double get_seconds() const { return time; }
	double get_milliseconds() const { return time * 1000.0; }

	auto operator<=>(const Timestep&) const = default;

private:
	double time = 0.0;
};
