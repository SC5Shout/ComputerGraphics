﻿#pragma once

#include <functional>

#include <SDL.h>
#include <imgui.h>
#include <imgui_impl_sdl.h>
#include <imgui_impl_opengl3.h>

#include "Timestep.h"
#include "Timer.h"
#include "Texture.h"
#include "Widgets.h"

struct ApplicationCreateInfo
{
	const char* name = "Window";
	const uint32_t w = 1280;
	const uint32_t h = 720;
	const int x = SDL_WINDOWPOS_CENTERED;
	const int y = SDL_WINDOWPOS_CENTERED;
};

struct Application
{
	~Application();

	Application(const Application&) = delete;
	Application& operator=(const Application&) = delete;

	Application(Application&& other) = delete;
	Application& operator=(Application&& other) = delete;

	[[nodiscard]] static inline Application& get()
	{
		static Application app;
		return app;
	}

	void run(const ApplicationCreateInfo& create_info);

	[[nodiscard]] inline SDL_Window* get_window() const { /*assert(initialized, "Application::get().get_window() has to be called ONLY FROM   on_XXX   functions");*/  return window; }

	std::function<void()> on_start;
	std::function<void(const SDL_Event)> on_event;
	std::function<void(const Timestep)> on_update;
	std::function<void()> on_draw;
	std::function<void()> on_gui;
	std::function<void()> on_exit;

	inline void set_docking_window_flags(const ImGuiWindowFlags flags) { /*assert(initialized, "Application::get().set_docking_window_flags() has to be called ONLY FROM   on_XXX   functions");*/ docking_window_flags = flags; }
	inline void add_docking_window_flags(const ImGuiWindowFlags flags) { /*assert(initialized, "Application::get().add_docking_window_flags() has to be called ONLY FROM   on_XXX   functions");*/ docking_window_flags |= flags; }
	inline void restore_default_docking_window_flags()
	{
		//assert(initialized, "Application::get().restore_default_docking_window_flags() has to be called ONLY FROM   on_XXX   functions");
		docking_window_flags = ImGuiWindowFlags_NoDocking;
		docking_window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		docking_window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
	}

private:
	template<typename FuncT>
	inline void RenderInADockspace(FuncT&& funcToRenderInADockspace)
	{
		static ImGuiDockNodeFlags opt_flags = ImGuiDockNodeFlags_None;

		ImGuiViewport* viewport = ImGui::GetMainViewport();

		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);

		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		Widget::ScopedWindow dockspace("MainDockspaceWindow", nullptr, docking_window_flags);
		ImGui::PopStyleVar(3);

		ImGuiIO& io = ImGui::GetIO();
		if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable) {
			ImGuiID dockspace_id = ImGui::GetID("MainDockspace");
			ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), opt_flags);
		}

		funcToRenderInADockspace();
	}

	Application() = default;
	void init(const ApplicationCreateInfo& create_info);

	Timer timer;
	Timestep delta_time;
	double last_time = 0.0;

	SDL_Window* window = nullptr;
	SDL_GLContext context = nullptr;
	ImGuiContext* imgui_context = nullptr;
	bool quit = false;

	bool initialized = false;

	ImGuiWindowFlags docking_window_flags;
};