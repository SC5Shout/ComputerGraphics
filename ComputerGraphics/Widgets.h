#pragma once

#include <utility>
#include <memory>

#include "imgui.h"
#include "imgui_internal.h"

#include "Texture.h"

template<typename T>
concept ImGuiAvailableColorTypes = std::is_same_v<T, uint32_t> || std::is_same_v<T, ImVec4>;

struct Widget
{
	struct ScopedStyle
	{
		template<typename StyleValue>
		ScopedStyle(ImGuiStyleVar style_var, StyleValue value) { ImGui::PushStyleVar(style_var, value); }
		~ScopedStyle() { ImGui::PopStyleVar(); }

		ScopedStyle(const ScopedStyle&) = delete;
		ScopedStyle operator=(const ScopedStyle&) = delete;
	};

	struct ScopedColor
	{
		ScopedColor(ImGuiCol color_id, ImGuiAvailableColorTypes auto color) { ImGui::PushStyleColor(color_id, color); }
		~ScopedColor() { ImGui::PopStyleColor(); }

		ScopedColor(const ScopedColor&) = delete;
		ScopedColor operator=(const ScopedColor&) = delete;
	};

	struct ScopedFont
	{
		ScopedFont(ImFont* font) { ImGui::PushFont(font); }
		~ScopedFont() { ImGui::PopFont(); }

		ScopedFont(const ScopedFont&) = delete;
		ScopedFont operator=(const ScopedFont&) = delete;
	};

	struct ScopedID
	{
		template<typename T>
		ScopedID(T id) { ImGui::PushID(id); }
		~ScopedID() { ImGui::PopID(); }

		ScopedID(const ScopedID&) = delete;
		ScopedID operator=(const ScopedID&) = delete;
	};

	struct ScopedWindow
	{
		ScopedWindow(const char* name, bool* open = nullptr, ImGuiWindowFlags flags = 0) { ImGui::Begin(name, open, flags); }
		~ScopedWindow() { ImGui::End(); }

		ScopedWindow(const ScopedWindow&) = delete;
		ScopedWindow operator=(const ScopedWindow&) = delete;
	};

	struct ScopedChildWindow
	{
		explicit ScopedChildWindow(const char* name, const ImVec2& size = ImVec2(0, 0), bool border = false, ImGuiWindowFlags flags = 0)
		{
			ImGui::BeginChild(name, size, border, flags);
		}
		~ScopedChildWindow() { ImGui::EndChild(); }

		ScopedChildWindow(const ScopedChildWindow&) = delete;
		ScopedChildWindow operator=(const ScopedChildWindow&) = delete;
	};

	struct ScopedTable
	{
		ScopedTable(const char* name, uint32_t columns_count, ImGuiTableFlags flags = 0, const ImVec2& outer_size = ImVec2(-FLT_MIN, 0), float inner_width = 0.0f)
		{
			ImGui::PushStyleVar(ImGuiStyleVar_CellPadding, ImVec2{ 2, 2 });
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(1.0f, 0.0f));
			ImGui::BeginTable(name, columns_count, flags, outer_size, inner_width);
		}
		~ScopedTable() { ImGui::EndTable(); ImGui::PopStyleVar(2); }

		ScopedTable(const ScopedTable&) = delete;
		ScopedTable operator=(const ScopedTable&) = delete;
	};

	//The count for multistyle is calculated as follow:
	//every ImGui::Push* contains 2 arguments - var/ID and value/color
	//The first var/ID and the value/color are treated as regular parameters so + 1 means one firstVar/ID and one firstValue/Color
	//Other* is treated as a variadic template pair which means that sizeof (Other*) is 2 times bigger
	//and to get the proper count the sizeof needs to be divided by 2

	//Maybe the builder'd be cleaner?
	struct ScopedMultiStyle
	{
		template <typename ValueType, typename... OtherStylePairs>
		ScopedMultiStyle(ImGuiStyleVar first_style_var, ValueType first_value, OtherStylePairs&& ... other_style_pairs)
			: count((sizeof... (other_style_pairs) / 2) + 1)
		{
			static_assert ((sizeof... (other_style_pairs) & 1u) == 0, "ScopedMultiStyle constructor expects a list of pairs of Vars and values as its arguments");

			PushStyle(first_style_var, first_value, std::forward<OtherStylePairs>(other_style_pairs)...);
		}

		~ScopedMultiStyle() { ImGui::PopStyleVar(count); }

		ScopedMultiStyle(const ScopedMultiStyle&) = delete;
		ScopedMultiStyle operator=(const ScopedMultiStyle&) = delete;

	private:
		template <typename ValueType, typename... OtherStylePairs>
		void PushStyle(ImGuiStyleVar style_var, ValueType value, OtherStylePairs&& ... other_style_pairs)
		{
			ImGui::PushStyleVar(style_var, value);
			if constexpr (sizeof... (other_style_pairs) != 0) {
				PushStyle(std::forward<OtherStylePairs>(other_style_pairs)...);
			}
		}

		uint32_t count = 0;
	};

	struct ScopedMultiColor
	{
		template<ImGuiAvailableColorTypes ... OtherColors>
		ScopedMultiColor(ImGuiCol first_color_id, ImGuiAvailableColorTypes auto first_color, OtherColors&& ... other_colors)
			: count((sizeof ... (other_colors)) / 2 + 1)
		{
			PushColor(first_color_id, first_color, std::forward<OtherColors>(other_colors)...);
		}

		~ScopedMultiColor() { ImGui::PopStyleColor(count); }

		ScopedMultiColor(const ScopedMultiStyle&) = delete;
		ScopedMultiColor operator=(const ScopedMultiStyle&) = delete;

	private:
		template <ImGuiAvailableColorTypes ... OtherColors>
		void PushColor(ImGuiCol color_id, ImGuiAvailableColorTypes auto value, OtherColors&& ... other_colors)
		{
			ImGui::PushStyleColor(color_id, value);
			if constexpr (sizeof... (other_colors) != 0) {
				PushColor(std::forward<OtherColors>(other_colors)...);
			}
		}

		uint32_t count = 0;
	};

	static inline bool ImageButton(const std::shared_ptr<Texture>& texture,
		const ImVec2& size,
		const ImVec2& uv0 = ImVec2(0, 0),
		const ImVec2& uv1 = ImVec2(1, 1),
		int frame_padding = -1,
		const ImVec4& bg_col = ImVec4(0, 0, 0, 0),
		const ImVec4& tint_col = ImVec4(1, 1, 1, 1))
	{
		return ImGui::ImageButton(*texture.get(), size, uv0, uv1, frame_padding, bg_col, tint_col);
	}

	static inline void Image(const std::shared_ptr<Texture>& texture,
		const ImVec2& size,
		const ImVec2& uv0 = ImVec2(0, 0),
		const ImVec2& uv1 = ImVec2(1, 1),
		const ImVec4& tint_col = ImVec4(1, 1, 1, 1),
		const ImVec4& border_col = ImVec4(0, 0, 0, 0))
	{
		ImGui::Image(*texture.get(), size, uv0, uv1, tint_col, border_col);
	}
};