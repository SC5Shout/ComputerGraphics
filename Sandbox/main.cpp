#include <iostream>
#include "Application.h"

int main(int argc, char* args[])
{
	bool button_clicked = false;
	int mouse_x = 0.0f;
	int mouse_y = 0.0f;

	Application::get().on_start = []() {
		std::cout << "The app has been initialized\n";
	};

	Application::get().on_gui = [&]() {
		Widget::ScopedWindow window("Window");
		if (ImGui::Button("Click me!")) {
			button_clicked = !button_clicked;
		}

		if (button_clicked) {
			ImGui::Text("Hello world!");
		}

		ImGui::Text("mouse X %i; mouse Y %i", mouse_x, mouse_y);
	};

	Application::get().on_event = [&](const SDL_Event event) {
		if (event.type == SDL_MOUSEMOTION) {
			mouse_x = event.motion.x;
			mouse_y = event.motion.y;
		}
	};

	Application::get().on_exit = []() {
		std::cout << "The app is about to be destroyed\n";
	};

	Application::get().run({});
	return 0;
}