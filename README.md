I've created this little project for Computer Graphics classes to avoid using QT + Qt Creator because it weights like 30GB or some like that.
It uses SDL2 + OpenGL + ImGui under the hood and is very lightweight. 

The main branch contains the library/framework code and other branches contain topics we talk about in class.

### Clone
`git clone https://gitlab.com/SC5Shout/ComputerGraphics.git`

### Build
Project uses some c++20 features, make sure to compile with c++20 flag on. 
Also remember to build for x64. 

Visual Studio

`mkdir build & cd build`

`cmake [options] ..`

CLion - just open the root directory in CLion and it will automatically build. 

### How to use
The application class provides a few customizable functions:

##User's functions should be called inside these functions.

- `on_start()` - runs after ImGui and SDL get created
- `on_event(const SDL_Event)` - runs when event occurs
- `on_update(const Timestep)` - runs once per frame, provies delta_time variable
- `on_draw()` - runs once per frame
- `on_gui()` - runs once per frame - every ImGui call should go there
- `on_exit()` - runs on exit before ImGui and SDL get destroyed

```cpp
#include <iostream>
#include "Application.h"

int main(int argc, char* args[])
{
	bool button_clicked = false;
	int mouse_x = 0.0f;
	int mouse_y = 0.0f;

	ApplicationCreateInfo create_info{
		.name = "My Application",
		.w = 800,
		.h = 600
	};

	Application::get().on_start = []() {
		std::cout << "The app has been initialized\n";
	};

	Application::get().on_gui = [&]() {
		Widget::ScopedWindow window("Window");
		if (ImGui::Button("Click me!")) {
			button_clicked = !button_clicked;
		}

		if (button_clicked) {
			ImGui::Text("Hello world!");
		}

		ImGui::Text("mouse X %i; mouse Y %i", mouse_x, mouse_y);
	};

	Application::get().on_event = [&](const SDL_Event event) {
		if (event.type == SDL_MOUSEMOTION) {
			mouse_x = event.motion.x;
			mouse_y = event.motion.y;
		}
	};

	Application::get().on_exit = []() {
		std::cout << "The app is about to be destroyed\n";
	};

	Application::get().run(create_info);
	return 0;
}
```

To work with texels, create a buffer, feed it with data, create a texture and add the data into the texture.
```cpp
std::shared_ptr<Texture> texture = nullptr;

//remember to create the texture inside a `on_XXX` function
Application::get().on_start = [&texture]() {
	//vector can be created outside of the `on_start` function, but then it'd has to be copied/moved into.
	std::vector<uint32_t> pixels(w * h);
	//some calculations...

	texture = Texture::create(std::move(pixels), w, h);
};

Application::get().on_gui = [&texture](){
	Widget::ScopedWindow window("Window");
        //Widget::Image is just a wrapper around ImGui::Image, it takes std::shared_ptr instead of ImTextureID, though.
	Widget::Image(texture, {w, h});
};
```

The project also provides a few RAII things around ImGui, most of them are straightforward
- `Widget::ScopedStyle` - `ImGui::PushStyleVar` in a ctor and `ImGui::PopStyleVar` in a dtor
- `Widget::ScopedColor` - `ImGui::PushStyleColor` in a ctor and `ImGui::PopStyleColor` in a dtor
- `Widget::ScopedFont` - `ImGui::PushFont` in a ctor and `ImGui::PopFont` in a dtor
- `Widget::ScopedID` - `ImGui::PushID` in a ctor and `ImGui::PopID` in a dtor
- `Widget::ScopedWindow` - `ImGui::Begin` in a ctor and `ImGui::End` in a dtor
- `Widget::ScopedChildWindow` - `ImGui::BeginChild` in a ctor and `ImGui::EndChild` in a dtor

example:
```cpp
{
        //style is pushed ImGui::PushStyleVar
	Widget::ScopedStyle style(ImGuiStyleVar_WindowRounding, 0.0f);
} // style is poped ImGui::PopStyleVar
```

`Widget::ScopedMultiStyle` and `Widget::ScopedMultiColor` are a bit trickier to implement, but still straightforward to use

example:
```cpp
{
	uint32_t buttonColors[3] = { 0xff350D99, 0xff5430EF,  0xff596FFF };
	ScopedMultiColor colors(ImGuiCol_Button, buttonColors[0],
							ImGuiCol_ButtonHovered, buttonColors[1],
							ImGuiCol_ButtonActive, buttonColors[2]);
	ImGui::Button("Custom colored button");
}
```